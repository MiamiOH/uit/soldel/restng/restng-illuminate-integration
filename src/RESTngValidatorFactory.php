<?php

namespace MiamiOH\RESTngIlluminateIntegration;

use Illuminate\Validation\Validator;
use Illuminate\Translation;
use Illuminate\Filesystem\Filesystem;

class RESTngValidatorFactory
{
    public static function make(array $data, array $rules, array $messages = [], array $customAttributes = []) : Validator
    {
        $validator =  new Validator(self::loadTranslator(), $data, $rules, $messages, $customAttributes);

        return $validator;
    }

    private static function loadTranslator()
    {
        $langPath = dirname(__FILE__) . '/lang';

        $filesystem = new Filesystem();

        $loader = new Translation\FileLoader(
            $filesystem,
            $langPath
        );

        $loader->addNamespace(
            'lang',
            $langPath
        );

        $loader->load('en', 'validation', 'lang');

        return new Translation\Translator($loader, 'en');
    }
}
