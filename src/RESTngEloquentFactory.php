<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/2/18
 * Time: 12:57 PM
 */

namespace MiamiOH\RESTngIlluminateIntegration;

use Illuminate\Database\Capsule\Manager as DB;
use Yajra\Oci8\Connectors\OracleConnector;
use Yajra\Oci8\Oci8Connection;

class RESTngEloquentFactory
{
    public static function boot(array $dataSourceConfig)
    {
        /*
         *  Documentation: https://yajrabox.com/docs/laravel-oci8/master/stand-alone
         */
        $db = new DB();

        $manager = $db->getDatabaseManager();

        $manager->extend('oracle', function ($config) {
            $connector = new OracleConnector();
            $connection = $connector->connect($config);
            $db = new Oci8Connection($connection, $config["database"], $config["prefix"]);

            // set oracle session variables
            $sessionVars = [
                'NLS_TIME_FORMAT' => 'HH24:MI:SS',
                'NLS_DATE_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                'NLS_NUMERIC_CHARACTERS' => '.,',
            ];

            // Like Postgres, Oracle allows the concept of "schema"
            if (isset($config['schema'])) {
                $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
            }

            $db->setSessionVars($sessionVars);

            return $db;
        });

        $db->addConnection([
            'driver' => $dataSourceConfig['driver'] ?? 'oracle',
            'tns' => $dataSourceConfig['database'] ?? '',
            'username' => $dataSourceConfig['user'] ?? '',
            'password' => $dataSourceConfig['password'] ?? '',
            'options' => [\PDO::ATTR_PERSISTENT => true],
            'host' => '',
            'port' => '',
            'database' => $dataSourceConfig['database'] ?? '',
            'prefix' => ''
        ]);

        $db->setAsGlobal();

        $db->bootEloquent();
    }
}
