<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/18/18
 * Time: 1:57 PM
 */

namespace Tests\Unit;

use Illuminate\Database\Schema\Blueprint;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use PHPUnit\Framework\TestCase;
use Illuminate\Database\Capsule\Manager as DB;
use Tests\EloquentModels\Foo as TestModel;

class EloquentTest extends TestCase
{
    private static $table = 'foo';

    public static function setUpBeforeClass()
    {
        $dataSourceConfig = [
          'driver' => 'sqlite',
          'database' => ':memory:'
        ];

        RESTngEloquentFactory::boot($dataSourceConfig);

        DB::schema()->create(self::$table, function (Blueprint $table) {
            $table->integer('id', 6);
            $table->string('code', 6);
            $table->string('desc', 255)->nullable();
        });
    }

    protected function tearDown()
    {
        DB::table(self::$table)->delete();
    }

    public function testTableExists()
    {
        $count = DB::table(self::$table)->count();

        $this->assertEquals($count, 0);
    }

    public function testInsertRecord()
    {
        DB::table(self::$table)->insert([
            ['id' => 1, 'code' => 'A', 'desc' => 'Hello'],
            ['id' => 2, 'code' => 'B', 'desc' => 'Hi'],
        ]);

        $count = DB::table(self::$table)->count();

        $this->assertEquals($count, 2);
    }

    public function testEloquentModelCount()
    {
        DB::table(self::$table)->insert([
            ['id' => 1, 'code' => 'A', 'desc' => 'Hello'],
            ['id' => 2, 'code' => 'B', 'desc' => 'Hi'],
        ]);

        $count = TestModel::count();

        $this->assertEquals($count, 2);
    }

    public function testEloquentModelSelect()
    {
        DB::table(self::$table)->insert([
            ['id' => 1, 'code' => 'A', 'desc' => 'Hello'],
            ['id' => 2, 'code' => 'B', 'desc' => 'Hi'],
        ]);

        $test1 = TestModel::find(1);

        $this->assertEquals($test1->code, 'A');
    }
}